package com.infoarmy.owler.sharing;

import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Sample sharing Application
 * 
 * @author Gautam
 * 
 */
public class OwlerSharing extends Activity {

	/**
	 * SocialAuth Adapter
	 */
	private SocialAuthAdapter mAdapter;
	/**
	 * status flag
	 */
	boolean mStatus;
	/**
	 * status EdiText
	 */
	private EditText mStatusEidtText;
	private LinearLayout linearBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		initalize();

	}

	/**
	 * Initailizes the views and the listeners
	 */

	private void initalize() {
		linearBar = (LinearLayout) findViewById(R.id.linearbar);
		linearBar.setBackgroundResource(R.drawable.bar_gradient);

		// Add Bar to library
		mAdapter = new SocialAuthAdapter(new ResponseListener());

		// Add providers
		mAdapter.addProvider(Provider.FACEBOOK, R.drawable.facebook);
		mAdapter.addProvider(Provider.TWITTER, R.drawable.twitter);
		mAdapter.addProvider(Provider.LINKEDIN, R.drawable.linkedin);

		mAdapter.addCallBack(Provider.TWITTER,
				"http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do");

		mAdapter.enable(linearBar);

	}

	@Override
	protected void onResume() {

		if (!isConnectedToInternet()) {
			Toast.makeText(OwlerSharing.this, R.string.enable_your_internet,
					Toast.LENGTH_LONG).show();
		}
		super.onResume();
	}

	/**
	 * Listens Response from Library
	 * 
	 */

	private final class ResponseListener implements DialogListener {

		@Override
		public void onComplete(Bundle values) {

			mStatusEidtText = (EditText) findViewById(R.id.editTxt);
			// Get name of provider after authentication
			final String providerName = values
					.getString(SocialAuthAdapter.PROVIDER);
			Toast.makeText(OwlerSharing.this, providerName + " connected",
					Toast.LENGTH_SHORT).show();
			if (mStatusEidtText.getText().toString() != null
					&& !mStatusEidtText.getText().toString().trim().equals("")) {

				mAdapter.updateStatus(mStatusEidtText.getText().toString(),
						new MessageListener());
			} else {
				Toast.makeText(OwlerSharing.this,
						getResources().getString(R.string.status_is_empty),
						Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		public void onError(SocialAuthError error) {
			error.printStackTrace();
			OwlerSharing.this.runOnUiThread(new Runnable() {
				public void run() {
					if (!isConnectedToInternet()) {
						Toast.makeText(
								OwlerSharing.this,
								getResources().getString(
										R.string.enable_your_internet),
								Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(
								OwlerSharing.this,
								getResources().getString(
										R.string.message_not_posted),
								Toast.LENGTH_LONG).show();
					}
				}
			});
		}

		@Override
		public void onCancel() {
		}

		@Override
		public void onBack() {
		}
	}

	private final class MessageListener implements SocialAuthListener<Integer> {
		@Override
		public void onExecute(Integer responseCode) {
			Integer status = responseCode;
			if (status.intValue() == 200 || status.intValue() == 201
					|| status.intValue() == 204)
				Toast.makeText(OwlerSharing.this,
						getResources().getString(R.string.message_posted),
						Toast.LENGTH_LONG).show();
			else
				Toast.makeText(OwlerSharing.this,
						getResources().getString(R.string.message_not_posted),
						Toast.LENGTH_LONG).show();
		}

		@Override
		public void onError(SocialAuthError e) {
		}
	}

	public boolean isConnectedToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}
}
